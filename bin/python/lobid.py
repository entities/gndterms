import requests

API = "https://lobid.org/gnd/search"
HEADERS = {'user-agent': 'entityXML: Labels2GND-Descriptors; mailto:sikora@sub.uni-goettingen.de'}

def qstring( label, field = None, ascii = False ):
    if ascii and field:
        label = f'{field}.{ascii}:{label}'
    elif not ascii and field:
        label = f'{field}:{label}'
    elif ascii and not field:
        label = f'{ascii}:{label}'
    return label 

def request( params ):
    req = requests.get(API, headers = HEADERS, params = params)
    return req.json()

def qresult(res, n = 1):
    items = res.get("totalItems")
    members = res.get("member")
    results = []
    for item in members[0:n]:
        gnd_uri = item.get("id")
        gnd_type = item.get("type")
        gnd_name = item.get("preferredName")
        results.append({ 
            "name" : gnd_name, 
            "uri": gnd_uri, 
            "type" : gnd_type 
        })
    return results