import requests
import lobid


if __name__ == '__main__':
    """
    req = requests.get(API, params= {
        "format" : "json",
        "q" : "preferredName:Göttingen"
    })

    res = req.json()
    items = res.get("totalItems")
    members = res.get("member")

    print(qstring("Göttingen", "preferredName"))

    for member in members:
        gnd_uri = member.get("id")
        gnd_type = member.get("type")
        gnd_name = member.get("preferredName")
        
        print(gnd_name, " (", gnd_uri, ") ", gnd_type, "\n")
    """
    
    """
    ############ ITERATE LABELS ############
    LABELS = [
        "Göttingen",
        "Hannover",
        "Zeichenlehrer"
    ]

    for item in LABELS:
        q = qstring(item, "preferredName")
        res = lobid_api({
            "format" : "json",
            "q" : q
        })

        items = res.get("totalItems")
        members = res.get("member")
        member = members[0]

        gnd_uri = member.get("id")
        gnd_type = member.get("type")
        gnd_name = member.get("preferredName")
        
        print(gnd_name, " (", gnd_uri, ") ", gnd_type, "\n")
    """
    
    ############ ITERATE TYPED LABELS ############
    LABELS = [
        ("Göttingen", "PlaceOrGeographicName"),
        ("Hannover", "PlaceOrGeographicName"),
        ("Zeichenlehrer", "SubjectHeading")
    ]

    for item in LABELS:
        q = lobid.qstring(item[0], "preferredName")
        qfilter = f'type:{item[1]}'
        res = lobid.request({
            "format" : "json",
            "filter" : qfilter,
            "q" : q
        })

        print( lobid.qresult(res) )
