<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#">

    <xsl:variable name="version" select="'0.5.0'"/>
    <xsl:variable name="file" select="tokenize(base-uri(), '/')[last()]"/>
    <xsl:variable name="current-date" select="format-date(current-date(), '[Y0001]-[M01]-[D01]')"/>
    <xsl:variable name="timestamp" select="current-dateTime()"/>
    <xsl:variable name="mapping" select="substring-before(base-uri(), $file)||'mapping.xml'"/>
    
    <xsl:output indent="yes"/>

    <xsl:template match="/">
        <xsl:variable name="mapping-labels">
            <xsl:try>
                <xsl:copy-of select="doc($mapping)//*:label"/>
                <xsl:catch/>
            </xsl:try>
        </xsl:variable>
        <xsl:variable name="labels">
            <labels>
                <xsl:copy-of select="$mapping-labels"></xsl:copy-of>
                <xsl:call-template name="collect-places"/>
                <xsl:call-template name="collect-affiliations"/>
                <xsl:call-template name="collect-functions"/>
                <xsl:call-template name="collect-professions"/>
            </labels>
        </xsl:variable>
        <mapping xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"> 
            <xsl:for-each select="$labels//*:label">
                <xsl:variable name="name" select="@name"/>
                <xsl:variable name="type" select="@type"/>
                <xsl:choose>
                    <xsl:when test="not(preceding-sibling::*:label[@name=$name][@type=$type])">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:otherwise/>
                </xsl:choose>
            </xsl:for-each>
        </mapping>
    </xsl:template>

    <xsl:template name="collect-places">
        <xsl:variable name="place-terms" select="distinct-values((//gndo:placeOfActivity/text(), //gndo:placeOfBirth/text(), //gndo:placeOfDeath/text()))"/>
        <xsl:for-each select="$place-terms">
            <label type="PlaceOrGeographicName" name="{normalize-space(.)}"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="collect-affiliations">
        <xsl:variable name="aff-terms" select="distinct-values(//gndo:affiliation/text())"/>
        <xsl:for-each select="$aff-terms">
            <label type="CorporateBody" name="{normalize-space(.)}"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="collect-functions">
        <xsl:variable name="funct-terms" select="distinct-values(//gndo:functionOrRole/text())"/>
        <xsl:for-each select="$funct-terms">
            <label type="SubjectHeading" name="{normalize-space(.)}"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="collect-professions">
        <xsl:variable name="prof-terms" select="distinct-values(//gndo:professionOrOccupation/text())"/>
        <xsl:for-each select="$prof-terms">
            <label type="SubjectHeading" name="{normalize-space(.)}"/>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
