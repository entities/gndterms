<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
    xmlns:lobid="gnd:lobid" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#">

    <xsl:variable name="version" select="'0.5.0'"/>
    <xsl:variable name="file" select="tokenize(base-uri(), '/')[last()]"/>
    <xsl:variable name="current-date" select="format-date(current-date(), '[Y0001]-[M01]-[D01]')"/>
    <xsl:variable name="timestamp" select="current-dateTime()"/>
    <xsl:variable name="mapping" select="substring-before(base-uri(), $file)||'mapping.xml'"/>
    <xsl:variable name="lobid" select="load-xquery-module(
        'gnd:lobid',
        map{'location-hints' : '../xquery/modules/lobid.xqm'}
    )"/>

    <xsl:output indent="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates/>   
    </xsl:template>

    <xsl:template match="gndo:placeOfActivity">
        <xsl:variable name="label" select="text()"/>
        <xsl:variable name="search" select="$lobid?functions(xs:QName('lobid:search-term'))?2($label, 2)"/>
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates select="node()"/>
            <xsl:copy-of select="$search"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
