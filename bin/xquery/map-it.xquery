xquery version "3.1";

import module namespace lobid = "gnd:lobid" at "modules/lobid.xqm";

declare namespace gtm = "gnd:terms";
declare namespace gndo = "https://d-nb.info/standards/elementset/gnd#";
declare namespace exml = "https://sub.uni-goettingen.de/met/standards/entity-xml#";
declare namespace foaf = "http://xmlns.com/foaf/0.1/";
declare namespace geo = "http://www.opengis.net/ont/geosparql#";
declare namespace owl = "http://www.w3.org/2002/07/owl#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare namespace functx = "http://www.functx.com";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method 'xml';

declare variable $local:ns := "https://sub.uni-goettingen.de/met/standards/entity-xml#";

(:-----{ FUNCTIONS }-----:)

declare function functx:distinct-nodes
  ( $nodes as node()* )  as node()* {

    for $seq in (1 to count($nodes))
    return $nodes[$seq][not(functx:is-node-in-sequence(
                                .,$nodes[position() < $seq]))]
} ;

declare function functx:is-node-in-sequence
  ( $node as node()? ,
    $seq as node()* )  as xs:boolean {

   some $nodeInSeq in $seq satisfies $nodeInSeq is $node
} ;

declare function functx:distinct-deep
  ( $nodes as node()* )  as node()* {

    for $seq in (1 to count($nodes))
    return $nodes[$seq][not(functx:is-node-in-sequence-deep-equal(
                          .,$nodes[position() < $seq]))]
 } ;
 
declare function functx:is-node-in-sequence-deep-equal
  ( $node as node()? ,
    $seq as node()* )  as xs:boolean {

   some $nodeInSeq in $seq satisfies deep-equal($nodeInSeq,$node)
} ;
 
declare function local:element( $node as node()*, $terms as node()* ) as node()* {
    element {QName(namespace-uri($node), $node/name())}{
        $node/@*[not(name() ="when")],
        attribute {"when"}{current-dateTime()},
        $terms
    }
};

declare function local:transform($nodes as node()*) as item()* {
    for $node in $nodes
    return 
        typeswitch($node)
            case text() return $node
            case comment() return $node
            case processing-instruction() return $node
            case element(exml:mapping) return 
                <mapping xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" xmlns:gndo="https://d-nb.info/standards/elementset/gnd#">
                { 
                    local:transform($node/node()[not(self::*:label)]), 
                    for $term in functx:distinct-deep($node//label)
                    return 
                        if ($term[not(child::*:term)]) 
                        then (
                            let $label := $term/@name
                            let $type := if ($term/@type) then ("type:"||$term/@type) else ()
                            
                            let $lobid-json := lobid:api-filtered($label, "preferredName", $type)
                            let $members := (for $i in 1 to 5 return lobid:terms($lobid-json, true(), $local:ns)[$i] )
                            
                            return local:element($term, $members)
                        )
                        else (
                            $term
                        )
                }
                </mapping>
            default return 
                element{QName(namespace-uri($node), $node/name())}{
                    $node/@*,
                    local:transform($node/node())
                }
};

(:-----{ SCRIPT BODY }-----:)
let $doc := .
return 
    local:transform($doc/node())