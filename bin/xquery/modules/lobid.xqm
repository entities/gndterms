xquery version "3.1";

module namespace lobid = "gnd:lobid";
declare namespace gndo = "https://d-nb.info/standards/elementset/gnd#";

declare variable $lobid:url := "https://lobid.org/gnd/search";

(:-----{ FUNCTIONS }-----:)
declare function lobid:api( $string as xs:string ) as item()* {
    json-doc(lobid:query-uri($string))
};

declare function lobid:api( $string as xs:string, $field as xs:string ) as item()* {
    json-doc(lobid:query-uri($string, $field))
};


declare function lobid:api-filtered( $string as xs:string, $filter as xs:string ) as item()* {
    let $url := lobid:query-uri($string)||"&amp;filter="||$filter
    return json-doc($url)
};

declare function lobid:api-filtered( $string as xs:string, $field as xs:string, $filter as xs:string ) as item()* {
    let $url := lobid:query-uri($string, $field)||"&amp;filter="||$filter
    return json-doc($url)
};


declare function lobid:terms( $json as item()*, $xml as xs:boolean?, $ns as xs:string? ) as item()* {
    let $members := $json?member
    let $ns := if (count($ns) > 0) then ($ns) else ("https://d-nb.info/standards/elementset/gnd#")
    return 
        if ( $xml ) 
        then(
            for $member in $members?*
            let $gnd-uri := $member("id")
            let $gnd-name := $member("preferredName")
            let $gnd-type := $member("type")
            
            return (
                element {QName( $ns, "term" )}{
                    attribute {"gndo:ref"}{$gnd-uri},
                    attribute {"type"}{string-join(($gnd-type?*), ' ')},
                    (:attribute {"name"}{$gnd-name},:)
                    $gnd-name
                }
            )
        )
        
        else (
            $members
        )
};


declare function lobid:query-uri( $string as xs:string, $field as xs:string ) as item()* {
    let $uri := string-join(($lobid:url, lobid:query(encode-for-uri($string), $field)), "?")
    let $uri := $uri||'&amp;format=json'
    return $uri
};

declare function lobid:query-uri( $string as xs:string ) as item()* {
    let $uri := string-join(($lobid:url, lobid:query(encode-for-uri($string), ())), "?")
    let $uri := $uri||'&amp;format=json'
    return $uri
};


declare function lobid:query( $string as xs:string, $field as xs:string? ) as item()* {
    if ( count($field) > 0 )
    
    then (
    "q="||$field||":"||$string
    ) 
    
    else (
    "q="||$string
    )
};


declare function lobid:search-term( $string as xs:string ) as item()* {
    let $request := lobid:api( $string, "preferredName" )
    let $terms-in-xml := (for $i in 1 to 5 return lobid:terms($request, true(), "https://sub.uni-goettingen.de/met/standards/entity-xml#")[$i] )
    return $terms-in-xml
};

declare function lobid:search-term( $string as xs:string, $n as xs:integer ) as item()* {
    let $request := lobid:api( $string, "preferredName" )
    let $terms-in-xml := (for $i in 1 to $n return lobid:terms($request, true(), "https://sub.uni-goettingen.de/met/standards/entity-xml#")[$i] )
    return $terms-in-xml
};





