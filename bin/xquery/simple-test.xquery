xquery version "3.1";


import module namespace lobid = "gnd:lobid" at "modules/lobid.xqm";

declare namespace gtm = "gnd:terms";
declare namespace gndo = "https://d-nb.info/standards/elementset/gnd#";
declare namespace foaf = "http://xmlns.com/foaf/0.1/";
declare namespace geo = "http://www.opengis.net/ont/geosparql#";
declare namespace owl = "http://www.w3.org/2002/07/owl#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method 'xml';


(:-----{ FUNCTIONS }-----:)
declare function local:element( $node as node()*, $terms as node()* ) as node()* {
    element {QName(namespace-uri($node), $node/name())}{
        $node/@*,
        $terms
    }
};

(:-----{ SCRIPT BODY }-----:)
let $label := "Göttingen"
let $ns := "https://sub.uni-goettingen.de/met/standards/entity-xml#"
(:let $request := lobid:api( $label, "preferredName" )
let $terms-in-xml := (for $i in 1 to 5 return lobid:terms($request, true(), "https://sub.uni-goettingen.de/met/standards/entity-xml#")[$i] )
:)let $simple-search := lobid:search-term("Göttingen")
return $simple-search