xquery version "3.1";


import module namespace lobid = "gnd:lobid" at "modules/lobid.xqm";

declare namespace gtm = "gnd:terms";
declare namespace gndo = "https://d-nb.info/standards/elementset/gnd#";
declare namespace foaf = "http://xmlns.com/foaf/0.1/";
declare namespace geo = "http://www.opengis.net/ont/geosparql#";
declare namespace owl = "http://www.w3.org/2002/07/owl#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method 'xml';

(:-----{ SCRIPT BODY }-----:)
let $json-doc := .
(:let $json-data := parse-json('[{"data" : "test"},{"uwe" : "blub"}]'):)
(:let $json-data := json-doc('https://lobid.org/gnd/search?q=preferredName%3AGöttingen&amp;format=json'):)

let $json := lobid:api-filtered("Göttingen", "preferredName", "type:PlaceOrGeographicName")
let $members := lobid:terms($json, true(), ())
return $members[1]