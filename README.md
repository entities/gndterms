# GND-Term Mapping Scripte
- **Ziel**: Automatisiertes Mapping von Labels auf GND-URIs bzw. GND-Deskriptoren
- **APIS**:
  - [LOBID GND Api](https://lobid.org/gnd/api)

## Sample Query
- `https://lobid.org/gnd/search?q=london&format=json`
- `https://lobid.org/gnd/search?q=preferredName%3AGöttingen&format=json`

## Python
In `bin/python` befinden sich Skripte, um das LOBID API anzusprechen.

## XQuery
In `bin/xquery` befindet sich die Datei `map-it.xquery`. Dieses Skript kann man einfach auf einer entityXML Mapping Ressource, wie z.B. `data/sample-labels.xml` ausführen. Man erhält dann eine neue Mapping Ressource, die dann um die entsprechenden Term aus der GND über das LOBID API erweitert wurde.

Diese Datei kann dann im nächsten Schritt aufgeräumt werden und als `mapping.xml` in das gleiche Verzeichnis, wie eine entityXML Ressource gelegt werden, um dann geladen zu werden.